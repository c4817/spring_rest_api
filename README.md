# REST API
> 2021-12-17 César Freire


## deploy

activate profile linux  
`$ export spring_profiles_active=<profile>`

activate profile windows  
`$ set spring_profiles_active=<profile>`

local run  
`$ ./mvnw spring-boot:run`

local run with profile  
`$ ./mvnw spring-boot:run -Dspring-boot.run.profiles=development`

run final solution  
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=development`

---

## build

clean target  
`$ ./mvnw clean`

target build  
`$ ./mvnw package`

#install gitlab-runner
$ sudo apk add gitlab-runner

sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "Kjwt3xpsgJKygdQDXegx" \
  --executor "shell" \
  --description "docker-lab-node1" \
  --tag-list "production_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"
